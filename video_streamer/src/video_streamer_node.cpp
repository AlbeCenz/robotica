//
// Created by alberto on 10/12/17.
//
#include <iostream>

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

// using char* instead of cv::String for compatibility with OpenCV 2.4
const char* KEYS = "{ help h usage ?  | | print this message }"
                   "{ loadFromFile   | | load images from file }";

int main(int argc, char *argv[]) {

   cv::CommandLineParser parser(argc, argv, KEYS);

   ros::init(argc, argv, "image_publisher");
   ros::NodeHandle nh;
   image_transport::ImageTransport it(nh);
   image_transport::Publisher pub = it.advertise("camera/image_raw", 1);

   // open video stream from file or device;
   cv::VideoCapture video;
   auto filePath = parser.get<std::string>("loadFromFile");   // not using cv::CommandLineParser::has() for compatibility with OpenCV 2.4
   if (!filePath.empty()) {
      if (!video.open(filePath)) {
         std::cout << "Error opening file " << filePath << std::endl;
         return 0;
      }
      std::cout << "Opened video file" << std::endl;
   }
   else {
      if (!video.open(0)) {
         std::cout << "Error opening video input device 0" << std::endl;
         return 0;
      }
      std::cout << "Opened video device" << std::endl;
   }

   // publish video stream
   std::cout << "Streaming on ros topic camera/image_raw" << std::endl;

   cv::Mat image;
   sensor_msgs::ImagePtr msg;
   ros::Rate loop_rate(25);  // 25Hz
   while (nh.ok()) {
      video >> image;
      if (image.empty()) {
         std::cout << "EOF reached! Closing stream and shutting down node." << std::endl;
         break;
      }
      msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image).toImageMsg();
      pub.publish(msg);
      ros::spinOnce();
      loop_rate.sleep();
   }

   return 0;
}